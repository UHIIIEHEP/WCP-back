import { Connection } from 'typeorm';
import { Application } from '../../entity/application.entity';

export const applicationProvider = [
  {
    provide: 'APPLICATION_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(Application),
    inject: ['DB_CONNECT'],
  },
]