import { Body, Controller, Post, SetMetadata, UseGuards } from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { UserGuard } from '../../decorator/guard';
import { Application } from '../../entity/application.entity';
import { ApplicationService } from './application.service';

@Controller('application')
export class ApplicationController {
  constructor(private readonly applicationService: ApplicationService) {}

  @Post('create')
  @ApiBearerAuth()
  @SetMetadata('permission', 'application.write')
  @UseGuards(UserGuard)
  async create(
    @Body() payload,
    ): Promise<{application: Application}> {
    return this.applicationService.create(payload);
  }
}
