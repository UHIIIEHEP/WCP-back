import { Module } from '@nestjs/common';
// import { DbModule } from '../../db/db.module';
import { userProvider } from '../user/user.provider';
import { ApplicationController } from './application.controller';
import { applicationProvider } from './application.provider';
import { ApplicationService } from './application.service';

@Module({
  imports: [
    // DbModule,
  ],
  controllers: [ApplicationController],
  providers: [
    ...userProvider,
    ...applicationProvider,
    ApplicationService,
  ]
})
export class ApplicationModule {}
