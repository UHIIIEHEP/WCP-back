import { Module } from '@nestjs/common';
import { DbModule } from '../../db/db.module';
import { userProvider } from '../user/user.provider';
import { PermissionController } from './permission.controller';
import { permissionProvider } from './permission.provider';
import { PermissionService } from './permission.service';

@Module({
  imports: [
    DbModule,
  ],
  controllers: [PermissionController],
  providers: [
    ...userProvider,
    ...permissionProvider,
    PermissionService,
  ]
})
export class PermissionModule {}
