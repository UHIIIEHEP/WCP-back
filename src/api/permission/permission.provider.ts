import { Connection } from 'typeorm';
import { Permission } from '../../entity/permission.entity';

export const permissionProvider = [
  {
    provide: 'PERMISSION_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(Permission),
    inject: ['DB_CONNECT'],
  },
]