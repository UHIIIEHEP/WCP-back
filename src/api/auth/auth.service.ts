import { Injectable } from '@nestjs/common';
import { AuthLoginRequestDTO } from '../../dto/auth.dto';
import { Request } from 'express';
import { dbQuery } from '../../helper';

const jwt = require('jsonwebtoken');

@Injectable()
export class AuthService {

    async login (
      payload: AuthLoginRequestDTO,
      req?: Request,
    ): Promise<{token: string}> {

      const {
        login,
        password: p_hash,
      } = payload;

      const address: string = req?.headers['host'] || '';
      const client: string = req?.headers['user-agent'] || '';

      const [session_id] = await dbQuery(
        'pkg_auth',
        'user_auth',
        [
          login,
          p_hash,
          address,
          client,
        ]
      )

      var token = await jwt.sign({ session_id }, process.env.USER_SIGNATURE);

      return {token};
    }
}
