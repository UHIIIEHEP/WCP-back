import { Body, Controller, Post, Req } from '@nestjs/common';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { AuthLoginRequestDTO } from '../../dto/auth.dto';
import { AuthService } from './auth.service';
import { Request } from 'express';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  @ApiTags('Auth')
  @ApiBody({ type: AuthLoginRequestDTO })
  async authLogin(
    @Req() req: Request,
    @Body() payload: AuthLoginRequestDTO,
  ) : Promise<{token: string}> {
    return this.authService.login(payload
      , req);
  }
}
