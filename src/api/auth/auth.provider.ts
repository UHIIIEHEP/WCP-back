import { Connection } from 'typeorm';
import { Session } from '../../entity/session.entity';

export const sessionProvider = [
  {
    provide: 'SESSION_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(Session),
    inject: ['DB_CONNECT'],
  },
]