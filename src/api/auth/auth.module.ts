import { Module } from '@nestjs/common';
import { DbModule } from '../../db/db.module';
import { AuthController } from './auth.controller';
import { sessionProvider } from './auth.provider';
import { AuthService } from './auth.service';

@Module({
  imports: [
    DbModule,
  ],
  controllers: [AuthController],
  providers: [
    ...sessionProvider,
    AuthService,
  ]
})
export class AuthModule {}
