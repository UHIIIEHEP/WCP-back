import { Body, Controller, Post, SetMetadata, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiOperation } from '@nestjs/swagger';
import { UserGuard } from '../../decorator/guard';
import { ElementCreateRequestDTO, ElementListRequestDTO } from '../../dto/element.dto';
import { Elements } from '../../entity/element.entity';
import { ElementService } from './element.service';

@Controller('element')
export class ElementController {
  constructor(private readonly elementService: ElementService) {}

  @Post('create')
  @ApiBearerAuth()
  @SetMetadata('permission', 'element.write')
  @UseGuards(UserGuard)
  @ApiBody({ type: ElementCreateRequestDTO })
  @ApiOperation({
    summary: 'ElementCreate',
    operationId: 'elementCreate',
  })
  async elementCreate(
    @Body() payload,
  ): Promise<{element: Elements}> {
    return this.elementService.create(payload);
  }

  @Post('list')
  @ApiBearerAuth()
  @SetMetadata('permission', 'element.read')
  @UseGuards(UserGuard)
  @ApiBody({ type: ElementListRequestDTO })
  @ApiOperation({
    summary: 'ElementList',
    operationId: 'elementList',
  })
  async elementList(
    @Body() payload,
  ): Promise<{element: Elements[]}> {
    return this.elementService.list(payload);
  }
}
