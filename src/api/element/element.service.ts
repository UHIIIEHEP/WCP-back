import { Injectable } from '@nestjs/common';
import { getConnection } from 'typeorm';
import { ElementCreateRequestDTO, ElementListRequestDTO } from '../../dto/element.dto';
import { Elements } from '../../entity/element.entity';

@Injectable()
export class ElementService {

  async create (
    payload: ElementCreateRequestDTO,
  ): Promise<{element: Elements}> {

    const {
      parent_id = null,
      job_id = null,
      name,
    } = payload;

    const data = await elementCreateCommon({
      parent_id,
      job_id,
      name,
    })

    const [element] = await elementListCommon({
      element_id: [data.raw[0].element_id],
    })

    return { element }
  }

  async list (
    payload: ElementListRequestDTO,
    ): Promise<{element: Elements[]}> {

    const {
      element_id = null,
    } = payload;

    const element = await elementListCommon({
      element_id,
    })

    return { element }
  }
}

const elementCreateCommon = async (params: {
  parent_id?: number;
  job_id?: number;
  name: string;
}): Promise<any> => {
  const {
    parent_id,
    job_id,
    name,
  } = params;

  return await getConnection()
    .createQueryBuilder()
    .insert()
    .into(Elements)
    .values([{
      parent_id,
      job_id,
      name,
    }])
    .execute()
}

const elementListCommon = async (params: {
  element_id?: number[];
}): Promise<any> => {
  if (!params.element_id || params.element_id.length === 0) {
    return await getConnection()
      .createQueryBuilder(Elements, 'e')
      .getMany();
  };

  return await getConnection()
    .createQueryBuilder(Elements, 'e')
    .where(`e.element_id in (:...element_id)`, {element_id: params.element_id})
    .getMany();
}
