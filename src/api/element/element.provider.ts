import { Connection } from 'typeorm';
import { Elements } from '../../entity/element.entity';

export const elementProvider = [
  {
    provide: 'ELEMENT_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(Elements),
    inject: ['DB_CONNECT'],
  },
]