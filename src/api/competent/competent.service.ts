import { Injectable } from '@nestjs/common';
import {
  CompetentListRequestDTO,
  CompetentRelationListRequestDTO,
  CompetentRelationSetRequestDTO,
  CompetentUserSelfListRequestDTO,
} from '../../dto/competent.dto';
import { Competent } from '../../entity/competent.entity';
import { dbQuery } from '../../helper';

@Injectable()
export class CompetentService {

  async list(
    payload: CompetentListRequestDTO,
  ): Promise<{competent: Competent[]}> {
    const {
      competent_id = null,
    } = payload;

    const competent = await competentListCommon({
      competent_id
    })

    return {competent};
  }

  async competentRelationSet(
    payload: CompetentRelationSetRequestDTO,
  ): Promise<boolean> {
    const {
      userInfoBySession: {user_id},
      competent_id,
      relation,
      relation_id,
      append = true,
    } = payload;

    await competentRelationSetCommon({
      user_id,
      competent_id,
      relation,
      relation_id,
      append,
    })

    return true;
  }

  async competentRelationGet(
    payload: CompetentRelationListRequestDTO,
  ): Promise<{competent: Competent[]}> {
    const {
      relation,
      relation_id,
    } = payload;

    const competent = await competentRelationListCommon({
      relation,
      relation_id,
    })

    return {competent};
  }

  async competentUserSelfGet(
    payload: CompetentUserSelfListRequestDTO,
  ): Promise<{competent: Competent[]}> {
    const {
      userInfoBySession: {user_id},
    } = payload;

    const competent = await competentRelationListCommon({
      relation: 'user',
      relation_id: user_id,
    })

    return {competent};
  }
}

const competentRelationSetCommon = async (params: {
  user_id: number,
  competent_id: number[],
  relation: string,
  relation_id: number,
  append: boolean,
}): Promise<any> => {

  const {
    user_id,
    competent_id,
    relation,
    relation_id,
    append,
  } = params;

  return await dbQuery(
    'pkg_competent',
    'competent_relation_set',
    [
      user_id,
      competent_id,
      relation,
      relation_id,
      append,
    ]
  )
}

const competentListCommon = async (params: {
  competent_id: number[]
}) => {
  return await dbQuery(
    'pkg_competent',
    'competent_list',
    [
      params.competent_id,
    ]
  )
}

const competentRelationListCommon = async (params: {
  relation: string,
  relation_id: number,
}): Promise<any> => {
  console.log({params})
  return await dbQuery(
    'pkg_competent',
    'competent_relation_list',
    [
      params.relation,
      params.relation_id,
    ]
  )
}
