import { Body, Controller, Post, SetMetadata, UseGuards } from '@nestjs/common';
import {
  ApiBearerAuth, ApiBody, ApiOperation, ApiTags,
} from '@nestjs/swagger';
import { UserGuard } from '../../decorator/guard';
import {
  CompetentRelationSetRequestDTO,
  CompetentRelationListRequestDTO,
  CompetentUserSelfListRequestDTO,
  CompetentListRequestDTO,
} from '../../dto/competent.dto';
import { Competent } from '../../entity/competent.entity';
import { CompetentService } from './competent.service';

@ApiTags('Competent')
@Controller('competent')
export class CompetentController {
  constructor(private readonly competentService: CompetentService) {}

  @Post('list')
  @ApiBearerAuth()
  @ApiBody({ type: CompetentListRequestDTO })
  @SetMetadata('permission', 'competent.read')
  @UseGuards(UserGuard)
  @ApiOperation({
    summary: 'CompetentList',
    operationId: 'competentList',
  })
  async competentList(
    @Body() payload,
  ): Promise<{competent: Competent[]}> {
    return this.competentService.list(payload);
  }

  @Post('relation/set')
  @ApiBearerAuth()
  @ApiBody({ type: CompetentRelationSetRequestDTO })
  @SetMetadata('permission', 'competent.relation.write')
  @UseGuards(UserGuard)
  @ApiOperation({
    summary: 'CompetentRelationSet',
    operationId: 'competentRelationSet',
  })
  async competentUserSet(
    @Body() payload: CompetentRelationSetRequestDTO,
  ): Promise<boolean> {
    return this.competentService.competentRelationSet(payload);
  }

  @Post('relation/get')
  @ApiBearerAuth()
  @ApiBody({ type: CompetentRelationListRequestDTO })
  @SetMetadata('permission', 'competent.relation.read')
  @UseGuards(UserGuard)
  @ApiOperation({
    summary: 'CompetentRelationSet',
    operationId: 'competentRelationGet',
  })
  async competentUserGet(
    @Body() payload: CompetentRelationListRequestDTO,
  ): Promise<{competent: Competent[]}> {
    return this.competentService.competentRelationGet(payload);
  }

  @Post('user/self/get')
  @ApiBearerAuth()
  @ApiBody({ type: CompetentUserSelfListRequestDTO })
  @SetMetadata('permission', 'competent.user.self.read')
  @UseGuards(UserGuard)
  @ApiOperation({
    summary: 'CompetentUserSelfGet',
    operationId: 'competentUserSelfGet',
  })
  async competentUserSelfGet(
    @Body() payload: CompetentUserSelfListRequestDTO,
  ): Promise<{competent: Competent[]}> {
    return this.competentService.competentUserSelfGet(payload);
  }
  
}
