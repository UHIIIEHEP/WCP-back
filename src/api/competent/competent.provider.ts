import { Connection } from 'typeorm';
import { Competent } from '../../entity/competent.entity';

export const competentProvider = [
  {
    provide: 'COMPETENT_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(Competent),
    inject: ['DB_CONNECT'],
  },
]