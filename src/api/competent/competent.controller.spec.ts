import { Test } from '@nestjs/testing';
import { DbModule } from '../../db/db.module';
import { userProvider } from '../user/user.provider';
import { UserService } from '../user/user.service';
import { CompetentController } from './competent.controller';
import { competentProvider } from './competent.provider';
import { CompetentService } from './competent.service';

describe('CompetentController', () => {
  let competentController: CompetentController;
  let competentService: CompetentService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        DbModule,
      ],
      controllers: [CompetentController],
      providers: [
        ...userProvider,
        ...competentProvider,
        UserService,
        CompetentService
      ],
      }).compile();

    competentService = moduleRef.get<CompetentService>(CompetentService);
    competentController = moduleRef.get<CompetentController>(CompetentController);
  });

  // describe('create', () => {
  //   it('create', async () => {

  //     expect(await competentController.competentList(payload)).toEqual(response);
  //   })
  // })

  describe('competentList', () => {
    it('should return an array of Competent', async () => {

      const userInfoBySession = {
        firstname: 'aaa',
        lastname: 'aaa',
        patronymic: 'aaa',
        email: 'aaa',
        login: 'aaa',
        organisation_id: 1,
        department_id: 1,
        qualification_id: 1,
        photo: 'aaa',
        user_id: 1,
      }

      const payload = {
        competent_id: [1],
        userInfoBySession,
      };

      const response = {
        competent:[
          {
            "competent_id": 1,
            "name": "ВК ТТ-220кВ и выше",
          },
        ],
      };

      expect(await competentController.competentList(payload)).toEqual(response);
    });
  });
});


// import { Test, TestingModule } from '@nestjs/testing';
// import { AuthService } from '../auth/auth.service';
// import { DbModule } from '../../db/db.module';
// import { sessionProvider } from '../auth/auth.provider';

// describe('AuthService', () => {
//   let service: AuthService;

//   beforeEach(async () => {
//     const module: TestingModule = await Test.createTestingModule({
//       providers: [AuthService, ...sessionProvider],
//       imports: [
//         DbModule,
//       ],
//     }).compile();

//     service = module.get<AuthService>(AuthService);
//   });

//   it('should be defined', async () => {
//     // expect(service).toBeDefined();
//     // jest.spyOn(service, 'login').mockImplementation(() => result);

//     const data = await service.login(
//       {
//         login: 'meamea',
//         password: '111111',
//       },
//     )

//     expect(typeof(data.token)).toBe('string');
//   });
// });
