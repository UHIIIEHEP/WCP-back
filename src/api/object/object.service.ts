import { BadRequestException, Injectable } from '@nestjs/common';
import { getConnection } from 'typeorm';
import { ObjectCreateRequestDTO, ObjectListRequestDTO } from '../../dto/object.dto';
import { Objects } from '../../entity/object.entity';

@Injectable()
export class ObjectService {

  async create (
    payload: ObjectCreateRequestDTO,
  ): Promise<{object: Objects}> {

    const {
      organisation_id,
      parent_id = null,
      element_id = null,
      name,
    } = payload;

    const data = await objectCreateCommon({
      organisation_id,
      parent_id,
      element_id,
      name,
    })

    const [object] = await objectListCommon({
      object_id: [data.raw[0].object_id],
    })

    return { object }
  }

  async list (
    payload: ObjectListRequestDTO,
  ): Promise<{object: Objects[]}> {

    const {
      organisation_id = null,
      object_id = null,
    } = payload;

    const object = await objectListCommon({
      organisation_id,
      object_id,
    })

    return { object }
  }
}

const objectCreateCommon = async (params: {
  organisation_id: number;
  parent_id?: number;
  element_id?: number;
  name: string;
}): Promise<any> => {
  const {
    organisation_id,
    parent_id,
    element_id,
    name,
  } = params;

  return await getConnection()
    .createQueryBuilder()
    .insert()
    .into(Objects)
    .values([{
      organisation_id,
      parent_id,
      element_id,
      name,
    }])
    .execute()
}

const objectListCommon = async (params: {
  organisation_id?: number;
  object_id?: number[];
}): Promise<any> => {

  if (params.organisation_id === null && params.object_id === null) {
    throw new BadRequestException('organisation_id and object_id is null')
  }
  
  if (params.organisation_id === null || params.organisation_id === undefined) {
    return await getConnection()
      .createQueryBuilder(Objects, 'o')
      .where('o.object_id in (:...object_id)', {object_id: params.object_id})
      .getMany();
  }
  
  if (params.object_id === null || params.object_id.length === 0) {
    return await getConnection()
      .createQueryBuilder(Objects, 'o')
      .where(`o.organisation_id = :organisation_id`, {organisation_id: params.organisation_id})
      .getMany();
  }
}
