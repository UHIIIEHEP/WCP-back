import {
  Body,
  Controller,
  Inject,
  Post,
  SetMetadata,
  UseGuards,
} from '@nestjs/common';
import {
  UserCreateRequestDTO,
} from '../../dto/user.dto';
import { UserService } from './user.service';
import {
  ApiTags,
  ApiBearerAuth,
} from '@nestjs/swagger';
import { User } from '../../entity/user.entity';
import { UserGuard } from '../../decorator/guard';
import { ClientProxy } from '@nestjs/microservices';

@ApiTags('User')
@Controller('user')
export class UserController {
  constructor(
    private readonly userService: UserService,
    // @Inject('MAILER_SERVICE') private readonly client: ClientProxy
    ) {}

  // async onApplicationBootstrap() {
  //   await this.client.connect();
  // }

  @Post('create')
  @ApiBearerAuth()
  @SetMetadata('permission', 'user.write')
  @UseGuards(UserGuard)
  async create(
    @Body() payload: UserCreateRequestDTO
  ): Promise<any> {

    const user = await this.userService.createUser(payload);
    // this.client.emit<any>('send_hello_mail', new Object({msg: user}));

    return user;
  }

  @Post('list')
  @ApiBearerAuth()
  @SetMetadata('permission', 'user.read')
  @UseGuards(UserGuard)
  async userList(
    @Body() payload,
  ): Promise<{user: User[]}> {
    return this.userService.list(payload);
  }

  @Post('self/info')
  @ApiBearerAuth()
  @SetMetadata('permission', 'user.self.info.read')
  @UseGuards(UserGuard)
  async selfInfo(
    @Body() payload,
  ): Promise<{user: User}> {
    return this.userService.selfInfo(payload);
  }
}
