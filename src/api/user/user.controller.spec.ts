import { Test } from '@nestjs/testing';
import { DbModule } from '../../db/db.module';
import { UserCreateRequestDTO } from '../../dto/user.dto';
import { msProvider } from '../../microservice.provider';
import { userProvider } from '../user/user.provider';
import { UserService } from '../user/user.service';
import { UserController } from './user.controller';

describe('UserController', () => {
  let userController: UserController;
  let userService: UserService;

  const userInfoBySession = {
    firstname: 'aaa',
    lastname: 'aaa',
    patronymic: 'aaa',
    email: 'aaa',
    login: 'aaa',
    password: '1',
    organisation_id: 3,
    department_id: 1,
    qualification_id: 1,
    photo: 'aaa',
    user_id: 1,
  }

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        DbModule,
        msProvider,
      ],
      controllers: [UserController],
      providers: [
        ...userProvider,
        UserService,
      ],
    }).compile();

    userService = moduleRef.get<UserService>(UserService);
    userController = moduleRef.get<UserController>(UserController);
  });

  // describe('create', () => {
  //   it('create', async () => {

  //     const payload: any = {
  //       login: 'ingenerEgor1',
  //       email: '9i-pe@list.ru',
  //       password: '111111',
  //       firstname: 'Егор',
  //       lastname: 'Мазнев',
  //       patronymic: 'Александрович',
  //       qualification_id: 1,
  //       department_id: 1,
  //       photo: null,
  //       userInfoBySession: {
  //         user_id: 1,
  //         organisation_id: 3,
  //         department_id: 1,
  //         qualification_id: 1,
  //         firstname: 'Egor',
  //         lastname: 'M',
  //         patronymic: 'A',
  //         email: 'mea@mail.ru'
  //       }
  //     };

  //     const response = {
  //       user:{
  //         user_id: 35,
  //         organisation_id: 3,
  //         firstname: 'Егор',
  //         lastname: 'Мазнев',
  //         patronymic: 'Александрович',
  //         email: '9i-pe@list.ru',
  //         department_id: 1,
  //         department_name: 'Отдел РЗА и АСУТП',
  //         qualification_id: 1,
  //         qualification_name: 'Инженер 2 категории',
  //         photo: null
  //       }
  //     };

  //     expect(await userController.create(payload)).toEqual(response);
  //   })
  // })

  describe('UserList', () => {
    it('should return an array of User', async () => {

      const payload = {
        user_id: [1],
        userInfoBySession,
      };

      const response = {
        user:[
          {
            user_id: 1,
            organisation_id: 3,
            firstname: 'Egor',
            lastname: 'M',
            patronymic: 'A',
            email: 'mea@mail.ru',
            department_id: 1,
            department_name: 'Отдел РЗА и АСУТП',
            qualification_id: 1,
            qualification_name: 'Инженер 2 категории',
            photo: null
          },
        ],
      };

      expect(await userController.userList(payload)).toEqual(response);
    });
  });
});


// import { Test, TestingModule } from '@nestjs/testing';
// import { AuthService } from '../auth/auth.service';
// import { DbModule } from '../../db/db.module';
// import { sessionProvider } from '../auth/auth.provider';

// describe('AuthService', () => {
//   let service: AuthService;

//   beforeEach(async () => {
//     const module: TestingModule = await Test.createTestingModule({
//       providers: [AuthService, ...sessionProvider],
//       imports: [
//         DbModule,
//       ],
//     }).compile();

//     service = module.get<AuthService>(AuthService);
//   });

//   it('should be defined', async () => {
//     // expect(service).toBeDefined();
//     // jest.spyOn(service, 'login').mockImplementation(() => result);

//     const data = await service.login(
//       {
//         login: 'meamea',
//         password: '111111',
//       },
//     )

//     expect(typeof(data.token)).toBe('string');
//   });
// });
