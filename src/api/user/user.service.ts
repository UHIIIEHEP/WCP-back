import { Injectable } from '@nestjs/common';
import { UserCreateRequestDTO, UserListRequestDTO } from '../../dto/user.dto';
import { User } from '../../entity/user.entity';
import { dbQuery } from '../../helper';

@Injectable()
export class UserService {

  async createUser(
    payload: UserCreateRequestDTO,
  ): Promise<{user: User}> {

    const {
      userInfoBySession: {
        user_id,
        organisation_id,
      },
      firstname,
      lastname,
      patronymic = null,
      email,
      login,
      password,
      department_id = null,
      qualification_id = null,
      photo = null,
    } = payload;

    const [result] = await dbQuery(
      'pkg_user',
      'user_create',
      [
        user_id,
        organisation_id,
        login,
        email,
        password,
        firstname,
        lastname,
        patronymic,
        qualification_id,
        department_id,
        photo,
      ],
    );

    const user_id_new = result.user_create;

    const [user] = await userListCommon({organisation_id, user_id: [user_id_new]})

    return {user};
  }

  async list(
    payload: UserListRequestDTO,
  ): Promise<{user: User[]}> {
    const {
      userInfoBySession: {
        organisation_id,
      },
      user_id = [],
    } = payload;

    const user = await userListCommon({
      organisation_id,
      user_id
    })

    return {user};
  }

  async selfInfo(
    payload: UserListRequestDTO,
  ): Promise<{user: User}> {
    const {
      userInfoBySession: {
        organisation_id,
        user_id,
      }
    } = payload;

    const [user] = await userListCommon({
      organisation_id,
      user_id: [user_id]
    })

    console.log({user})

    return {user};
  }
}

const userListCommon = async (params: {
  organisation_id: number,
  user_id: number[],
}) => {
  const {
    organisation_id,
    user_id,
  } = params;

  return await dbQuery(
    'pkg_user',
    'user_list',
    [
      organisation_id,
      user_id
    ],
  );
}