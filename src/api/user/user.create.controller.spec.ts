import { Test } from '@nestjs/testing';
import { DbModule } from '../../db/db.module';
import { msProvider } from '../../microservice.provider';
import { userProvider } from '../user/user.provider';
import { UserService } from '../user/user.service';
import { UserController } from './user.controller';

const faker = require('faker');

describe('UserController', () => {
  let userController: UserController;
  let userService: UserService;

  const email = faker.internet.email();
  const login = faker.name.firstName();
  const firstname = faker.name.firstName();
  const lastname = faker.name.lastName();
  const patronymic = faker.name.middleName();
  const qualification_id = 1;
  const department_id = 1;
  const photo = null;
  const organisation_id = 1;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        DbModule,
        msProvider,
      ],
      controllers: [UserController],
      providers: [
        ...userProvider,
        UserService,
      ],
    }).compile();

    userService = moduleRef.get<UserService>(UserService);
    userController = moduleRef.get<UserController>(UserController);
  });

  describe('create', () => {
    it('create', async () => {

      const payload: any = {
        login,
        email,
        password: '111111',
        firstname,
        lastname,
        patronymic,
        qualification_id,
        department_id,
        photo,
        userInfoBySession: {
          user_id: 1,
          organisation_id,
          department_id: 1,
          qualification_id: 1,
          firstname: 'Egor',
          lastname: 'M',
          patronymic: 'A',
          email: 'mea@mail.ru'
        }
      };

      const response = await userController.create(payload);

      const {user} = response;

      expect(user.firstname).toEqual(firstname);
      expect(user.lastname).toEqual(lastname);
      expect(user.patronymic).toEqual(patronymic);
      expect(user.organisation_id).toEqual(organisation_id);
      expect(user.email).toEqual(email);
      expect(user.photo).toEqual(photo);
    })
  })
});
