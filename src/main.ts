import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { AllExceptionsFilter } from './filter/allExceptionsFilter.filter';
// import { Transport } from '@nestjs/microservices';

require ('dotenv').config();

const port: number = Number(process.env.APP_PORT) || 4000;

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors();

  app.useGlobalPipes(new ValidationPipe());

  app.useGlobalFilters(new AllExceptionsFilter());

  await app.startAllMicroservicesAsync();

  const config = new DocumentBuilder()
    .setTitle('Work Managment Platform')
    .setDescription('Work Managment Platform')
    .setVersion('1.0')
    // .addTag('cats')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  await app.listen(port, () => {
    console.log(`Port: ${port}`)
  });
}

bootstrap();
