import { Module } from '@nestjs/common';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { UserModule } from './api/user/user.module';
import { AuthModule } from './api/auth/auth.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DbModule } from './db/db.module';
import { ResponseInterceptor } from './interceptor/response.interceptor';
import { AuthService } from './api/auth/auth.service';
import { userProvider } from './api/user/user.provider';
import { CompetentModule } from './api/competent/competent.module';
import { ObjectModule } from './api/object/object.module';
import { ElementModule } from './api/element/element.module';
import { PermissionModule } from './api/permission/permission.module';
import { TypeOrmModule } from '@nestjs/typeorm';


const provide = 'DB_CONNECT';
const type = 'postgres';
const host = process.env.APP_DB_HOST;
const port = Number(process.env.APP_DB_PORT);
const username = process.env.APP_DB_USERNAME;
const password = process.env.APP_DB_PASSWORD;
const database = process.env.APP_DB_NAME;

@Module({
  imports: [
    AuthModule,
    UserModule,
    CompetentModule,
    // ObjectModule,
    // ElementModule,
    PermissionModule,
    DbModule,
    TypeOrmModule.forRoot({type,
      host,
      port,
      username,
      password,
      database,
      entities: [`${__dirname}/../**/*.entity{.ts,.js}`],
      synchronize: false,
    }),
  ],
  controllers: [AppController],
  providers: [
    ...userProvider,
    AppService,
    AuthService,
    {
      provide: APP_INTERCEPTOR,
      useClass: ResponseInterceptor,
    },
  ],
})
export class AppModule {}
