const paramHandler = (
  param: string,
  index: Number,
  log: { [key: string]: any },
  ): string => {
  return param;
};

export const filterError = (log: any): any => {
  if (typeof log === 'object' && log !== null && Array.isArray(log?.parameters))
    return {
      ...log,
      parameters: log.parameters
        .map((param, index) => paramHandler(param, index, log)),
    };
  return log;
};
