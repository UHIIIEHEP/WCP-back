import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
  HttpStatus,
  BadRequestException,
  ForbiddenException,
  NotFoundException,
  UnauthorizedException,
  MethodNotAllowedException,
} from '@nestjs/common';

import { DatabaseException } from '../exception/databaseException.exception';
import { AppException } from '../exception/AppException.exception';
import { filterError } from './error.filter';

// const { logMsg } = require('../k8s_lib/log');

const isProduction = (): boolean => process.env.NODE_ENV === 'production';

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  catch(exception: any, host: ArgumentsHost) {
    const context = host.switchToHttp();
    const res = context.getResponse();
    const req = context.getRequest();

    const errorCode =
      exception instanceof HttpException
        ? exception.getStatus()
        : exception.statusCode || HttpStatus.INTERNAL_SERVER_ERROR;

    const xRequestId = req.headers['x-request-id'] || 'requestid';

    let logMessage;
    let responseMessage;
    let typeError;

    // instanceof switch workaround https://stackoverflow.com/a/54286277
    switch (true) {

      case exception instanceof AppException:
        typeError = 'appError';

        logMessage = exception.error;

        // response is the same on production
        responseMessage = logMessage;
        break;

      case exception instanceof DatabaseException:
        typeError = 'db_error';

        logMessage = exception.error;

        // return only message on production else full error
        responseMessage =  isProduction() ? exception.error.message : exception.error;
        break;

      case (
        // list of expected exceptions
        exception instanceof BadRequestException ||
        exception instanceof ForbiddenException ||
        exception instanceof NotFoundException ||
        exception instanceof UnauthorizedException ||
        exception instanceof MethodNotAllowedException
      ):
        typeError = HttpStatus[errorCode].toLocaleLowerCase();

        logMessage = exception.response?.message || exception.message;
        if (Array.isArray(logMessage) && logMessage.length === 1) {
          [ logMessage ] = logMessage;
        }

        // response is the same on production
        responseMessage = logMessage;
        break;

      default:
        typeError = HttpStatus[errorCode].toLocaleLowerCase();

        logMessage = exception.message || exception;

        // response is the same on production
        responseMessage = logMessage;
        break;
    }

    const response = {
      status: 'error',
      errorCode,
      typeError,
      timestamp: new Date().toISOString(),
      path: req.url,
      'x-request-id': xRequestId,
      error: filterError(responseMessage),
    };

    console.log(
      filterError(logMessage),
    );

    // logMsg(
    //   true,
    //   filterError(logMessage),
    //   req.headers['x-request-id'],
    //   req.headers['request-session-id'],
    // );

    res.status(errorCode).json(response);
  }
}
