import * as jwt from 'jsonwebtoken';

const isJwtToken = (jwtToken: string): boolean => {
  const jwtSignature = [
    process.env.APP_ADMIN_JWT_SIGNATURE,
    process.env.APP_USER_JWT_SIGNATURE,
  ];

  let isToken = false;

  jwtSignature.forEach(secret => {
    try {
      jwt.verify(jwtToken, secret);
      isToken = true;
    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        // do nothing
      }
    }
  });
  return isToken;
};

export const filterResponseLog = (log: { result: any }): { result: any } => {
  if (typeof log.result === 'object') {
    const { token } = log.result;
    if (token && isJwtToken(token)) {
      return { ...log, result: { ...log.result, token: '***' } };
    }
  }

  return log;
};
