import { HttpException, HttpStatus } from '@nestjs/common';

export class AppException extends HttpException {
  public error: object;

  constructor(error?: string | object | any) {
    super('Some error', HttpStatus.BAD_REQUEST);
    this.error = error;
  }
}
