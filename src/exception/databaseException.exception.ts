import { HttpException, HttpStatus } from '@nestjs/common';

export class DatabaseException extends HttpException {
  public error: object;

  constructor(error?: string | object | any) {
    super('Database error', HttpStatus.BAD_REQUEST);
    this.error = error;
  }
}
