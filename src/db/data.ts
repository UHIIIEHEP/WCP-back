const permission = [
  {name: 'Просмотр квалификации', alias: 'qualifacation.read'},
  {name: 'Просмотр пользователй', alias: 'user.read'},
  {name: 'Просмотр своей квалификации', alias: 'qualifacation.self.read'},
  {name: 'Просмотр информации о себе', alias: 'user.self.info.read'},
  {name: 'Просмотр компетенций', alias: 'competent.user.read'},
  {name: 'Просмотр своих компетенций', alias: 'competent.user.self.read'},
  {name: 'Установить компетенции сущности', alias: 'competent.relation.write'},
  {name: 'Просмотр компетенции сущности', alias: 'competent.relation.read'},
]
