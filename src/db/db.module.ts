import { Module, Session } from '@nestjs/common';
import { dbProviders } from './db.providers';

@Module({
  providers: [...dbProviders],
  exports: [...dbProviders],
})
export class DbModule {}
