import { createConnection, getConnection, getConnectionOptions } from 'typeorm';

const dbQuery = async (pkg: string, foo: string, params: Array<any>) => {
    let queryParams = '';

    if (params.length !== 0 ) {
        params.forEach((elem, index) => {
            let val = '';
            if (Array.isArray(elem)) {
                if (elem.length !== 0) {
                    val = `Array[${elem.join(',')}]`;
                } else {
                    val = 'null';
                }

            } else {
                val = typeof(elem) === 'string' ? `'${elem}'`: elem;
            }

            queryParams += index !== 0 ? `, ${val}`: `${val}`;
        });
    };

    try {
        const result = await getConnection().query(`select * from ${pkg}.${foo}(${queryParams})`);

        return result;
    } catch (err) {
        console.log('Error dbQuery', {err})
        throw new Error(err);
    }
}

export {
    dbQuery,
}
