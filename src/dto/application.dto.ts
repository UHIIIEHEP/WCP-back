import { ApiProperty } from "@nestjs/swagger";
import { IsDate, IsInt, IsOptional, IsString } from "class-validator";
import { UserInfoBySession } from "./user.dto";


export class ApplicationCreateRequestDTO extends UserInfoBySession {
  @ApiProperty({
    example: null,
    required: false,
  })
  @IsInt()
  @IsOptional()
  parent_id: number;

  @ApiProperty({
    example: 'equipment_in',
  })
  @IsString()
  type: string;

  @ApiProperty({
    example: 'Вывод в ремонт 1ШС',
  })
  @IsString()
  name: string;

  @ApiProperty({
    example: 'описание',
  })
  @IsString()
  description: string;

  @ApiProperty({
    example: 1,
  })
  @IsDate()
  object_id: number;

  @ApiProperty({
    example: '2021-01-01',
  })
  @IsDate()
  date_start: Date;

  @ApiProperty({
    example: '2021-02-01',
  })
  @IsDate()
  date_finish: Date;
}

export class ApplicationResponseDTO extends ApplicationCreateRequestDTO{
  @ApiProperty({
    example: 1
  })
  application_id: number;

  @ApiProperty({
    example: '2021-01-01',
  })
  @IsDate()
  created: Date;

  @ApiProperty({
    example: 1,
  })
  @IsDate()
  created_by: number;
}

export class ApplicationResponseOkDTO {
  @ApiProperty()
  status: 'ok';

  @ApiProperty()
  result: ApplicationResponseDTO;
}