import { ApiProperty } from "@nestjs/swagger";
import { IsInt, IsPositive } from "class-validator";

export class ElementCreateRequestDTO {
  parent_id: number;
  job_id: number;
  name: string;
}

export class ElementListRequestDTO {
  @ApiProperty({
    example: [1],
    isArray: true,
  })
  @IsInt()
  @IsPositive()
  element_id: number[];
}

export class ElementResponseDTO {
  @ApiProperty({
    example: 1
  })
  element_id: number;

  @ApiProperty({
    example: 1
  })
  parent_id: number;

  @ApiProperty({
    example: 1
  })
  job_id: number;

  @ApiProperty({
    example: 'ТТ-220кВ ВЛ-220кВ ПС-1 - ПС-2'
  })
  name: string;
}
