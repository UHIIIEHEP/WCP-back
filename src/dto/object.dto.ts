export class ObjectCreateRequestDTO {
  organisation_id: number;
  parent_id: number;
  element_id: number;
  name: string;
}

export class ObjectListRequestDTO {
  organisation_id: number;
  object_id: number[];
}

export class ObjectResponseDTO {
  object_id: number;
  organisation_id: number;
  parent_id: number;
  element_id: number;
  name: string;
}
