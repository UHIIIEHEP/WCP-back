import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsString, IsNotEmpty } from "class-validator";

class Auth {
  login: string;
  password: string;
}

export class AuthLoginRequestDTO {
  @ApiProperty()
  login: string;

  @ApiProperty()
  password: string;
}

export class AuthResponseDTO {
  token: string;
}
