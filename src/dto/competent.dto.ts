import { ApiProperty } from "@nestjs/swagger";
import { IsArray, IsBoolean, IsInt, IsPositive, IsString } from "class-validator";
import { UserInfoBySession } from "./user.dto";

export class CompetentRelationListRequestDTO extends UserInfoBySession {
  @ApiProperty({
    example: 'user',
  })
  @IsString()
  relation: string;

  @ApiProperty({
    example: 1,
  })
  @IsInt()
  @IsPositive()
  relation_id: number;
}

export class CompetentUserSelfListRequestDTO extends UserInfoBySession {
}

export class CompetentRelationSetRequestDTO extends UserInfoBySession {
  @ApiProperty({
    example: [1],
    isArray: true,
  })
  @IsArray()
  competent_id: number[];

  @ApiProperty({
    example: 'user',
  })
  @IsString()
  relation: string;

  @ApiProperty({
    example: 1,
  })
  @IsInt()
  relation_id: number;

  @ApiProperty({
    example: true,
    required: false,
  })
  @IsBoolean()
  append: boolean;
}

export class CompetentListRequestDTO extends UserInfoBySession {
  @ApiProperty({
    example: [1],
    required: false,
  })
  @IsInt()
  competent_id: number[];
}

export class CompetentResponseDTO {
  @ApiProperty({
    example: 1,
  })
  competent_id: number;

  @ApiProperty({
    example: 'ВК ТТ-220кВ и выше',
  })
  name: string;
}
