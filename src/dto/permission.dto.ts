import { UserInfoBySession } from "./user.dto";



class Permission {
  name: string;
  alias: string;
}

export class PermissionCreateRequestDTO extends UserInfoBySession {
  name: string;
  alias: string;
}

export class PermissionListRequestDTO {
  permission_id: number[];
}

export class PermissionSetRequestDTO extends UserInfoBySession {
  permission_id: number[];
  department_id: number;
  qualification_id: number;
  append: boolean;
}

export class PermissionGetRequestDTO {
  user_id: number;
}

export class PermissionSelfGetRequestDTO extends UserInfoBySession {
}

export class PermissionListResponseDTO {
  permission_id: number;
  name: string;
  alias: string;  

}

export class PermissionResponseDTO {
  permission: PermissionListResponseDTO[];
}