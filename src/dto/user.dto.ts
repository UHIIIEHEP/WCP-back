import { ApiProperty } from '@nestjs/swagger';
import {
  IsString,
  IsInt,
  IsOptional,
  IsArray,
} from 'class-validator';

export class UserInfoBySession{
  userInfoBySession: UserInfo
}

class User extends UserInfoBySession {
  firstname: string;
  lastname: string;
  patronymic: string;
  email: string;
  login: string;
  organisation_id: number;
  department_id: number;
  qualification_id: number;
  photo: string;
}

class UserInfo extends User{
  user_id: number;
}

export class UserGuardDTO extends User {
  
}

export class UserCreateRequestDTO extends User{
  password: string;
}

export class UserListRequestDTO extends UserInfoBySession {
  user_id: number[];
}

export class UserListResponseDTO extends User {
  user_id: number;
}

export class UserResponseDTO {
  user: UserListResponseDTO[];
}
