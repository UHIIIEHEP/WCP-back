import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('qualification')
export class Qualification {
  @PrimaryGeneratedColumn()
  qualification_id: number;

  @Column('character varying')
  name: string;
}