import {
  PrimaryGeneratedColumn,
  Column,
  Entity,
} from "typeorm";

@Entity('session')
export class Session {
  @PrimaryGeneratedColumn()
  session_id: number;

  @Column('timestamp without time zone')
  created: string;

  @Column('integer')
  user_id: number;

  @Column('character varying')
  address: string;

  @Column('character varying')
  client: string;
}