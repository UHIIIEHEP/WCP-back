import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('competent')
export class Competent {
  @PrimaryGeneratedColumn()
  competent_id: number;

  @Column('character varying')
  name: string;
}
