import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToOne,
  JoinColumn,
  ManyToOne,
} from 'typeorm';
import { UserDepartmentQualificationSet } from './user_department_qualification_set.entity';

@Entity('user')
export class User {
  @PrimaryGeneratedColumn()
  user_id: number;

  @Column('character varying')
  firstname: string;

  @Column('character varying')
  lastname: string;

  @Column('character varying')
  patronymic: string;

  @Column('character varying')
  email: string;

  @Column('character varying')
  login: string;

  @Column('character varying')
  password: string;

  // @OneToOne(() => UserDepartmentQualificationSet)
  // @JoinColumn([{ name: 'user_id' }])
  // udqs: UserDepartmentQualificationSet;
}
