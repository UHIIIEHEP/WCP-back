import { Column, Entity, JoinColumn, ManyToOne, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { Competent } from "./competent.entity";

@Entity('competent_relation_set')
export class CompetentRelationSet {
  @PrimaryGeneratedColumn()
  competent_relation_set_id: number;

  @Column('integer')
  competent_id: number;

  @Column('character varying')
  relation: string;

  @Column('integer')
  relation_id: number;

  @ManyToOne(() => Competent, (competent) => competent.competent_id)
  @JoinColumn([{name: 'competent_id'}])
  competent: Competent;
}
