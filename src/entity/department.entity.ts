import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('department')
export class Department {
  @PrimaryGeneratedColumn()
  department_id: number;

  @Column('character varying')
  name: string;
}
