import { Column, Entity, JoinColumn, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { Department } from "./department.entity";
import { Qualification } from "./qualification.entity";
import { User } from "./user.entity";

@Entity('user_department_qualification_set')
export class UserDepartmentQualificationSet {
  @PrimaryGeneratedColumn()
  user_department_qualification_set_id: number;

  @Column('integer')
  department_id: number;

  @Column('integer')
  qualification_id: number;

  @Column('integer')
  user_id: number;

  // @OneToOne(() => User, user => user.user_id)
  // user: User

  @OneToOne(() => Department)
  @JoinColumn([{name: 'department_id'}])
  department: Department;

  @OneToOne(() => Qualification)
  @JoinColumn([{name: 'qualification_id'}])
  qualification: Qualification;
}
