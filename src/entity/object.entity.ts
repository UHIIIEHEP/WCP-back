import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('object')
export class Objects {
  @PrimaryGeneratedColumn()
  object_id: number;

  @Column('integer')
  organisation_id: number;

  @Column({ nullable: true, type: 'integer' })
  parent_id?: number;

  @Column({ nullable: true, type: 'integer' })
  element_id: number;

  @Column('character varying')
  name: string;
}
