/* ********* OLD PROCEDURE PARSER *********
const Str = require('string');

const parseFuncReturnResult = (res, argName = 'pg_get_function_result') => {
  let funcReturnArgsStr = res[0][argName];

  let result;

  if (funcReturnArgsStr.substr(0, 5).toLowerCase() !== 'table') {
    result = res[0][argName];
  } else {
    funcReturnArgsStr = funcReturnArgsStr.replace(/(\W+)f_/g, '$1');

    const values = funcReturnArgsStr.substr(6, funcReturnArgsStr.length - 7);

    result = Str(values)
      .parseCSV()
      .reduce((acc, field) => {
        const splits = field.split(' ');
        acc[splits[0]] = splits.slice(1).join(' ');
        return acc;
      }, {});
  }

  return result;
};

const procValue = (type, value) => {
  switch (type) {
    case 'numeric': // decimals
    case 'real':
    case 'double precision': {
      if (value === null || value === 'null' || value === '') {
        return null;
      }
      return Str(value).toFloat();
    }
    case 'smallint':
    case 'bigint':
    case 'integer': {
      if (value === null || value === 'null' || value === '') {
        return null;
      }

      return Str(value).toInt();
    }
    case 'boolean': {
      if (value === null || value === 'null' || value === '') {
        return null;
      }
      return value === 't';
    }
    case 'json': {
      try {
        return JSON.parse(value);
      } catch (e) {
        return {};
      }
    }
    default: {
      if (!value || !value.length) {
        return '';
      }

      return Str(value).toString();
    }
  }
};

 ********* END OLD PROCEDURE PARSER ********* */

const pgAssoc = data => {
  const names = Object.keys(data[0]);
  const values = data.map(row => Object.values(row));
  return [names, values];
};

/*
const pgUnAssoc = src => {
  const fieldNames = src[0];
  return src[1].reduce((srcResult, srcValue) => {
    const item = {};

    srcValue.reduce((itemResult, itemValue, itemIndex) => {
      item[fieldNames[itemIndex]] = itemValue;
      return itemResult;
    }, {});

    srcResult.push(item);
    return srcResult;
  }, []);
};

*/

export { pgAssoc };
