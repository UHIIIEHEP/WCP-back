require('dotenv').config()

const username = process.env.DB_USERNAME;
const host = process.env.DB_HOST;
const database = process.env.DB_DATABASE;
const password = process.env.DB_PASSWORD;
const port = Number(process.env.DB_PORT);

const config = {
  name: "default",
  type: "postgres",
  host,
  port,
  username,
  password,
  database,
};

export default config;
